/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esi.atl.g41406.paint.view;

import esi.atl.g41406.paint.model.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ToolBar;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

/**
 *
 * @author G41406
 */

public class App {
    
    final int RESERVED_HEIGHT = 65;
    
    private final VectorInt windowSize;
    
    private GridPane root;
    
    private ToolBar toolBar;
    
    private MenuBar menuBar;
    
    private Pane paintArea;
    
    private Point mouseOrigin;
    
    //private EventHandler<MouseEvent>{
    
//};
    
    private int mode;
    
    private Drawing drawing;
    
 
    public App(VectorInt winSize){
        this.windowSize = winSize;
        mouseOrigin = new Point();
        mode = 0;
        
        initContainer();
        fillMenuBar();
        fillToolBar();
        
        
        drawing.add(new Circle(50,50,50));
        drawing.add(new Square(200,200,30));
        draw();
        //drawing.getShape(0).move(100, 100);
        //this.move(new Point(50,50), new Point(400,400));
        draw();
        
    }
    public void initContainer(){
        root = new GridPane();
            
        menuBar = new MenuBar();
        toolBar = new ToolBar();
        paintArea = new Pane();
        
        drawing = new Drawing(windowSize.getX(),windowSize.getY()-RESERVED_HEIGHT);
            
        menuBar.setStyle("-fx-background-color: #aaa;");
        toolBar.setStyle("-fx-background-color: #ccc;");
        paintArea.setStyle("-fx-background-color: #aa0;");

        paintArea.setPrefHeight(windowSize.getY()-RESERVED_HEIGHT);
        paintArea.setPrefWidth(windowSize.getX());
            
        GridPane.setRowIndex(menuBar,0);
        GridPane.setColumnIndex(menuBar,0);
        GridPane.setRowIndex(toolBar,1);
        GridPane.setColumnIndex(toolBar,0);
        GridPane.setRowIndex(paintArea,2);
        GridPane.setColumnIndex(paintArea,0);
        
        root.getChildren().addAll(menuBar,toolBar,paintArea);
    }
    public void fillMenuBar(){
        
        Menu menuFile = new Menu("File");
        
        Menu menuHelp = new Menu("?");
        
        
        MenuItem exit = new MenuItem("Exit");
        
        exit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                System.exit(0);
            }
        });
        
        MenuItem help = new MenuItem("HELP");
        help.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
            }
        });
        
        menuHelp.getItems().add(help);
        menuFile.getItems().add(exit);
        
        menuBar.getMenus().addAll(menuFile,menuHelp);
    }
    public void fillToolBar(){
        
        Button modeCircle = new Button("Circle");
        
        modeCircle.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mode = 1;
            }
        });
        
        
        Button modeRectangle = new Button("Rectangle");
        
        modeRectangle.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mode = 2;
            }
        });
        
        
        Button modeMove = new Button("Move");
        
        modeMove.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mode = 3;
            }
        });
        
        
        Button modeRemove = new Button("Remove");
        modeRemove.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mode = 4;
            }
        });
        
        toolBar.getItems().addAll(modeCircle,modeRectangle,modeMove,modeRemove);
    }
    public void draw(){
       
        for(int i=0;i<drawing.getShapes().size();++i){
            if(drawing.getShape(i) instanceof Circle){
                Circle c = (Circle) drawing.getShape(i);
                javafx.scene.shape.Circle circle = new javafx.scene.shape.Circle(c.getCenter().getX(),c.getCenter().getY(),c.getRadius());
                circle.setOnDragDetected(this::startDragAndDrop);
                circle.setOnDragDropped(this::endDragAndDrop);
                paintArea.getChildren().add(circle);
                
            } else if(drawing.getShape(i) instanceof Rectangle || drawing.getShape(i) instanceof Square){
                Rectangle r = (Rectangle) drawing.getShape(i);
                javafx.scene.shape.Rectangle rectangle = new javafx.scene.shape.Rectangle(r.getOrigin().getX(),r.getOrigin().getY(),r.getWidth(),r.getHeight());
                
                paintArea.getChildren().add(rectangle);
            }
        }
    }
    public Pane getRoot(){
        return root;
    }
    
    public void move(Point loc,Point newLoc){
        boolean success = true;
        int id = 0;
        try {
            id = drawing.moveShapeAt(loc, newLoc);
        } catch (Exception ex) {
            success = false;
        }
        if(success){
            Point move = loc.getDistance(newLoc);
            paintArea.getChildren().get(id).setTranslateX(move.getX());
            paintArea.getChildren().get(id).setTranslateY(move.getY());
        }
        
    }
    private void startDragAndDrop(MouseEvent event) {
        if(mode == 3){
            mouseOrigin = new Point(event.getX(),event.getY());
            
            System.out.println(mouseOrigin);
            event.consume();
        }
    }
    private void endDragAndDrop(DragEvent event) {
        if(mode == 3){
            move(mouseOrigin,new Point(event.getX(),event.getY()));
            
            System.out.println("OK " + new Point(event.getX(),event.getY()));
        }
    }
    
    
}
