/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esi.atl.g41406.paint.view;

import esi.atl.g41406.paint.model.*;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author G41406
 */

public class ATL2G_PROJET_ABTS extends Application {
    final int WINDOW_WIDTH = 800;
    final int WINDOW_HEIGHT = 600;
    @Override
    public void start(Stage primaryStage) {
        App app = new App(new VectorInt(WINDOW_WIDTH,WINDOW_HEIGHT));
  
        
        Scene scene = new Scene(app.getRoot(), WINDOW_WIDTH, WINDOW_HEIGHT);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
